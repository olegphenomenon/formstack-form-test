package formstack.tests;

import formstack.pages.FormstackFormPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FormstackFormTest {
    WebDriver driver;
    FormstackFormPage page;

    String validEmailError = "Please enter a valid email address";
    String tooLittlePassError = "Must be at least 6 characters in length";
    String tooBigPassError = "Must be 12 characters or less in length";
    String requiredFieldError = "This field is required";

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        page = new FormstackFormPage(driver);
        driver.get("https://my.insuresign.com/#/signup");
    }

    @Test
    public void successfulTest() {
        page.
                typeFirstName("Elesey").
                typeLastName("Ciba").
                typeEmail("ciba@mail.ru").
                typePassword("234567").
                typeCompanyName("Ciba-Ciba").
                typePhoneNumber("1234567890").
                setAgreementCheckbox().
                clickConfirmButtonn();
        Assert.assertTrue(page.errorsCount() < 1);
    }

    @Test
    public void wrongEmail() {
        page.
                typeFirstName("Elesey").
                typeLastName("Ciba").
                typeEmail("ciba").
                typePassword("234567").
                typeCompanyName("Ciba-Ciba").
                typePhoneNumber("1234567890").
                setAgreementCheckbox().
                clickConfirmButtonn();
        Assert.assertEquals(page.getErrorMessageText(validEmailError),validEmailError);
    }

    @Test
    public void tooLittlePassError() {
        page.
                typeFirstName("Elesey").
                typeLastName("Ciba").
                typeEmail("ciba@mail.ru").
                typePassword("234").
                typeCompanyName("Ciba-Ciba").
                typePhoneNumber("1234567890").
                setAgreementCheckbox().
                clickConfirmButtonn();
        Assert.assertEquals(page.getErrorMessageText(tooLittlePassError),tooLittlePassError);
    }

    @Test
    public void tooBigPassError() {
        page.
                typeFirstName("Elesey").
                typeLastName("Ciba").
                typeEmail("ciba@mail.ru").
                typePassword("234423423424344234").
                typeCompanyName("Ciba-Ciba").
                typePhoneNumber("1234567890").
                setAgreementCheckbox().
                clickConfirmButtonn();
        Assert.assertEquals(page.getErrorMessageText(tooBigPassError),tooBigPassError);
    }

    @Test
    public void notEnoughPhoneNumber() {
        page.
                typeFirstName("Elesey").
                typeLastName("Ciba").
                typeEmail("ciba@mail.ru").
                typePassword("2343312").
                typeCompanyName("Ciba-Ciba").
                typePhoneNumber("1234").
                setAgreementCheckbox().
                clickConfirmButtonn();
        Assert.assertEquals(page.getErrorMessageText(requiredFieldError),requiredFieldError);
    }

    @Test
    public void checkHidePassword() {
        page.typePassword("12312313").clickShowPassword();
        Assert.assertFalse(page.isPasswordHidden());
    }

    @Test
    public void emptyFields() {
        page.
                typeFirstName("").
                typeLastName("").
                typeEmail("").
                typePassword("").
                typeCompanyName("").
                typePhoneNumber("");
        Assert.assertTrue(page.isTheButtonDisabled());
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
