package formstack.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FormstackFormPage {
    WebDriver driver;

    //Describe basic elements of form
    private By firstNameInput = By.xpath("//input[@name='firstName']");
    private By lastNameInput = By.xpath("//input[@name='lastName']");
    private By emailInput = By.xpath("//input[@name='email']");
    private By passwordInput = By.xpath("//input[@name='password']");
    private By companyNameInput = By.xpath("//input[@name='organization']");
    private By phoneNumberInput = By.xpath("//input[@name='phone']");
    private By agreementCheckbox = By.xpath("//input[@name='TOS']");
    private By showPasswordButton = By.xpath("//button[@class='btn btn-primary ng-binding']");
    private By confirmButton = By.xpath("//button[@class='btn btn-login btn-block signup-btn-large']");

    //Describe form errors
    private By errorLabel = By.xpath("//div[@class='error signup-error pull-right ng-scope']");
    private String errorMsgText = "//h6[text()='%s']";

    public FormstackFormPage(WebDriver driver) {
        this.driver = driver;
    }

    //Define methods for form elements
    public FormstackFormPage typeFirstName(String firstName) {
        driver.findElement(firstNameInput).clear();
        driver.findElement(firstNameInput).sendKeys(firstName);
        return this;
    }

    public FormstackFormPage typeLastName(String lastName) {
        driver.findElement(lastNameInput).clear();
        driver.findElement(lastNameInput).sendKeys(lastName);
        return this;
    }

    public FormstackFormPage typeEmail(String email) {
        driver.findElement(emailInput).clear();
        driver.findElement(emailInput).sendKeys(email);
        return this;
    }

    public FormstackFormPage typePassword(String password) {
        driver.findElement(passwordInput).clear();
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public FormstackFormPage typeCompanyName(String companyName) {
        driver.findElement(companyNameInput).clear();
        driver.findElement(companyNameInput).sendKeys(companyName);
        return this;
    }

    public FormstackFormPage typePhoneNumber(String phoneNumber) {
        driver.findElement(phoneNumberInput).clear();
        driver.findElement(phoneNumberInput).sendKeys(phoneNumber);
        return this;
    }

    public FormstackFormPage setAgreementCheckbox() {
        driver.findElement(agreementCheckbox).click();
        return this;
    }

    public FormstackFormPage clickShowPassword() {
        driver.findElement(showPasswordButton).click();
        return this;
    }

    public FormstackFormPage clickConfirmButtonn() {
        driver.findElement(confirmButton).click();
        return this;
    }

    public boolean isPasswordHidden() {
        return driver.findElement(passwordInput).getAttribute("type") == "password";
    }

    //check disable button
    public boolean isTheButtonDisabled() {
        return driver.findElement(confirmButton).isDisplayed();
    }

    //Method get text of error message
    public String getErrorMessageText(String errMsg) {
        return driver.findElement(By.xpath(String.format(errorMsgText,  errMsg))).getText();
    }

    public int errorsCount() {
        return driver.findElements(errorLabel).size();
    }

}
